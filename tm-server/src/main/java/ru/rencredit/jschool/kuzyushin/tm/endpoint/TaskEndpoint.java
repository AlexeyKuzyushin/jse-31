package ru.rencredit.jschool.kuzyushin.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.rencredit.jschool.kuzyushin.tm.api.endpoint.ITaskEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.api.service.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.api.service.ITaskService;
import ru.rencredit.jschool.kuzyushin.tm.dto.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.dto.TaskDTO;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

@WebService
@Controller
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @Nullable
    @Autowired
    private ITaskService taskService;

    public TaskEndpoint(final @NotNull ISessionService sessionService) {
        super(sessionService);
    }

    @Override
    @WebMethod
    public Long countAllTasks(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO) {
        sessionService.validate(sessionDTO, Role.ADMIN);
        return taskService.count();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public List<TaskDTO> findAllTasks(
            @WebParam(name = "session", partName = "session")  final @Nullable SessionDTO sessionDTO) {
        sessionService.validate(sessionDTO, Role.ADMIN);
        return taskService.findAll();
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDTO> findAllTasksByUserId(
            @WebParam(name = "session", partName = "session")  final @Nullable SessionDTO sessionDTO) {
        sessionService.validate(sessionDTO);
        return taskService.findAll(sessionDTO.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDTO> findAllTasksByProjectId(
            @WebParam(name = "session", partName = "session")  final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "projectId", partName = "projectId")  final @Nullable String projectId) {
        sessionService.validate(sessionDTO);
        return taskService.findAll(sessionDTO.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public void clearTasks(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        taskService.clear();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void createTask(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "projectId", partName = "projectId")  final @Nullable String projectId,
            @WebParam(name = "name", partName = "name") final @Nullable String name,
            @WebParam(name = "description", partName = "description") final @Nullable String description) {
        sessionService.validate(sessionDTO);
        taskService.create(sessionDTO.getUserId(), projectId, name, description);
    }

    @Override
    @Nullable
    @WebMethod
    public TaskDTO findTaskById(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") final @Nullable String id) {
        sessionService.validate(sessionDTO);
        return TaskDTO.toDTO(taskService.findById(sessionDTO.getUserId(), id));
    }

    @Override
    @Nullable
    @WebMethod
    public TaskDTO findTaskByName(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "name", partName = "name") final @Nullable String name) {
        sessionService.validate(sessionDTO);
        return TaskDTO.toDTO(taskService.findByName(sessionDTO.getUserId(), name));
    }

    @Override
    @WebMethod
    public void removeTaskById(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") final @Nullable String id) {
        sessionService.validate(sessionDTO);
        taskService.removeById(sessionDTO.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeTaskByName(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "name", partName = "name") final @Nullable String name) {
        sessionService.validate(sessionDTO);
        taskService.removeByName(sessionDTO.getUserId(), name);
    }

    @Override
    @WebMethod
    public void removeAllTasksByUserId(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO) {
        sessionService.validate(sessionDTO);
        taskService.removeAllByUserId(sessionDTO.getUserId());
    }

    @Override
    @WebMethod
    public void removeAllTasksByProjectId(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "projectId", partName = "projectId")  final @Nullable String projectId) {
        sessionService.validate(sessionDTO);
        taskService.removeAllByProjectId(projectId);
    }

    @NotNull
    @Override
    @WebMethod
    public Task updateTaskById(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") final @Nullable String id,
            @WebParam(name = "name", partName = "name") final @Nullable String name,
            @WebParam(name = "description", partName = "description") final @Nullable String description) {
        sessionService.validate(sessionDTO);
        return taskService.updateById(sessionDTO.getUserId(), id, name, description);
    }

    @Override
    @WebMethod
    public void updateTaskStartDate(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") final @Nullable String id,
            @WebParam(name = "date", partName = "date") final @Nullable Date date) {
        sessionService.validate(sessionDTO);
        taskService.updateFinishDate(sessionDTO.getUserId(), id, date);
    }

    @Override
    @WebMethod
    public void updateTaskFinishDate(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") final @Nullable String id,
            @WebParam(name = "date", partName = "date") final @Nullable Date date) {
        sessionService.validate(sessionDTO);
        taskService.updateFinishDate(sessionDTO.getUserId(), id, date);
    }
}
