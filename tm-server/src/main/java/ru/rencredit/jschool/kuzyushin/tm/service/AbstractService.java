package ru.rencredit.jschool.kuzyushin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IEntityManagerService;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IService;
import ru.rencredit.jschool.kuzyushin.tm.entity.AbstractEntity;

import javax.persistence.EntityManager;

public abstract class AbstractService <T extends AbstractEntity> implements IService<T> {

    protected final IEntityManagerService entityManagerService;

    public AbstractService(@NotNull final IEntityManagerService entityManagerService) {
        this.entityManagerService = entityManagerService;
    }
}
