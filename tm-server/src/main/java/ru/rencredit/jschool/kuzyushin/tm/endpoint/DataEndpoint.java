package ru.rencredit.jschool.kuzyushin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.rencredit.jschool.kuzyushin.tm.api.endpoint.IDataEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IDataService;
import ru.rencredit.jschool.kuzyushin.tm.api.service.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.dto.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Controller
public final class DataEndpoint extends AbstractEndpoint implements IDataEndpoint {

    @Nullable
    @Autowired
    private IDataService dataService;

    public DataEndpoint(final @NotNull ISessionService sessionService) {
        super(sessionService);
    }

    @Override
    @WebMethod
    public void saveDataBinary(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        dataService.saveDataBinary();
    }

    @Override
    @WebMethod
    public void saveDataJson(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        dataService.saveDataJson();
    }

    @Override
    @WebMethod
    public void loadDataBinary(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        dataService.loadDataBinary();
    }

    @Override
    @WebMethod
    public void loadDataJson(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        dataService.loadDataJson();
    }

    @Override
    @WebMethod
    public void clearDataBinary(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        dataService.clearDataBinary();
    }

    @Override
    @WebMethod
    public void clearDataJson(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        dataService.clearDataJson();
    }

    @Override
    @WebMethod
    public void saveDataXml(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        dataService.saveDataXml();
    }

    @Override
    @WebMethod
    public void loadDataXml(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        dataService.loadDataXml();
    }

    @Override
    @WebMethod
    public void clearDataXml(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        dataService.clearDataXml();
    }
}
