package ru.rencredit.jschool.kuzyushin.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.rencredit.jschool.kuzyushin.tm.api.endpoint.IProjectEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IProjectService;
import ru.rencredit.jschool.kuzyushin.tm.api.service.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.dto.ProjectDTO;
import ru.rencredit.jschool.kuzyushin.tm.dto.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

@WebService
@Controller
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @Nullable
    @Autowired
    private IProjectService projectService;

    public ProjectEndpoint(final @NotNull ISessionService sessionService) {
        super(sessionService);
    }

    @Override
    @WebMethod
    public Long countAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO) {
        sessionService.validate(sessionDTO, Role.ADMIN);
        return projectService.count();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public List<ProjectDTO> findAllProjects(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO) {
        sessionService.validate(sessionDTO, Role.ADMIN);
        return projectService.findAll();
    }

    @NotNull
    @Override
    @WebMethod
    public  List<ProjectDTO> findAllProjectsByUserId(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO) {
        sessionService.validate(sessionDTO);
        return projectService.findAllByUserId(sessionDTO.getUserId());
    }

    @Override
    @WebMethod
    public void clearProjects(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO) {
        sessionService.validate(sessionDTO, Role.ADMIN);
        projectService.clear();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void createProject(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "name", partName = "name") final @Nullable String name,
            @WebParam(name = "description", partName = "description") @Nullable String description) {
        sessionService.validate(sessionDTO);
        projectService.create(sessionDTO.getUserId(), name, description);
    }

    @Override
    @Nullable
    @WebMethod
    public ProjectDTO findProjectById(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") final @Nullable String id) {
        sessionService.validate(sessionDTO);
        return ProjectDTO.toDTO(projectService.findById(sessionDTO.getUserId(), id));
    }

    @Override
    @Nullable
    @WebMethod
    public ProjectDTO findProjectByName(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "name", partName = "name") final @Nullable String name) {
        sessionService.validate(sessionDTO);
        return ProjectDTO.toDTO(projectService.findByName(sessionDTO.getUserId(), name));
    }

    @Override
    @WebMethod
    public void removeProjectById(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") final @Nullable String id) {
        sessionService.validate(sessionDTO);
        projectService.removeById(sessionDTO.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeProjectByName(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "name", partName = "name") final @Nullable String name) {
        sessionService.validate(sessionDTO);
        projectService.removeByName(sessionDTO.getUserId(), name);
    }

    @Override
    @WebMethod
    public void removeAllProjectsByUserId(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO) {
        sessionService.validate(sessionDTO);
        projectService.removeAllByUserId(sessionDTO.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public ProjectDTO updateProjectById(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") final @Nullable String id,
            @WebParam(name = "name", partName = "name") final @Nullable String name,
            @WebParam(name = "description", partName = "description") final @Nullable String description) {
        sessionService.validate(sessionDTO);
        return ProjectDTO.toDTO(projectService.updateById(sessionDTO.getUserId(), id,
                name, description));
    }

    @Override
    @WebMethod
    public void updateProjectStartDate(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") final @Nullable String id,
            @WebParam(name = "date", partName = "date") @Nullable final Date date) {
        sessionService.validate(sessionDTO);
        projectService.updateFinishDate(sessionDTO.getUserId(), id, date);
    }

    @Override
    @WebMethod
    public void updateProjectFinishDate(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") final @Nullable String id,
            @WebParam(name = "date", partName = "date") final @Nullable Date date) {
        sessionService.validate(sessionDTO);
        projectService.updateFinishDate(sessionDTO.getUserId(), id, date);
    }
}
