package ru.rencredit.jschool.kuzyushin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.rencredit.jschool.kuzyushin.tm.api.endpoint.ISessionEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IPropertyService;
import ru.rencredit.jschool.kuzyushin.tm.api.service.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.dto.Fail;
import ru.rencredit.jschool.kuzyushin.tm.dto.Result;
import ru.rencredit.jschool.kuzyushin.tm.dto.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.dto.Success;
import ru.rencredit.jschool.kuzyushin.tm.exception.user.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Controller
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @Nullable
    @Autowired
    private IPropertyService propertyService;

    public SessionEndpoint(final @NotNull ISessionService sessionService) {
        super(sessionService);
    }

    @Override
    @WebMethod
    public SessionDTO openSession(
            @WebParam(name = "login", partName = "login") final @Nullable String login,
            @WebParam(name = "password", partName = "password") final @Nullable String password) {
        return sessionService.open(login, password);
    }

    @NotNull
    @Override
    @WebMethod
    public Result closeSession(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO
    ) throws AccessDeniedException {
        sessionService.validate(sessionDTO);
        try {
            sessionService.close(sessionDTO);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result closeAllUserSession(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO
    ) throws AccessDeniedException {
        sessionService.validate(sessionDTO);
        try {
            sessionService.closeAll(sessionDTO);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Nullable
    @Override
    @WebMethod
    public String getServerHost(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO ) {
        sessionService.validate(sessionDTO);
        return propertyService.getServerHost();
    }

    @Nullable
    @Override
    @WebMethod
    public Integer getServerPort(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO ) {
        sessionService.validate(sessionDTO);
        return propertyService.getServerPort();
    }

}
