package ru.rencredit.jschool.kuzyushin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.boostrap.ClientBootstrap;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;

import java.util.List;

@Component
public final class ShowCmdCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private ClientBootstrap clientBootstrap;

    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @NotNull
    @Override
    public String arg() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String description() {
        return "Show application commands";
    }

    @Override
    public void execute() {
        @NotNull final List<AbstractCommand> commands = clientBootstrap.getCommandList();
        for (@NotNull final AbstractCommand command: commands)
            if (command.arg() != null)
                System.out.println(command.name());
    }
}
