package ru.rencredit.jschool.kuzyushin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.ProjectEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;

@Component
public final class ProjectRemoveAllByUserId extends AbstractCommand {

    @NotNull
    private final ProjectEndpoint projectEndpoint;

    @Autowired
    public ProjectRemoveAllByUserId(
            final @NotNull ProjectEndpoint projectEndpoint,
            final @NotNull ISessionService sessionService
    ) {
        super(sessionService);
        this.projectEndpoint = projectEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "project-remove-all-by-userId";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all projects by user";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE All PROJECT]");
            @Nullable SessionDTO sessionDTO = sessionService.getCurrentSession();
            projectEndpoint.removeAllProjectsByUserId(sessionDTO);
            System.out.println("[OK]");
    }
}
