package ru.rencredit.jschool.kuzyushin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.ProjectDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.ProjectEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;

import java.util.List;

@Component
public final class ProjectListCommand extends AbstractCommand {

    @NotNull
    private final ProjectEndpoint projectEndpoint;

    @Autowired
    public ProjectListCommand(
            final @NotNull ProjectEndpoint projectEndpoint,
            final @NotNull ISessionService sessionService
    ) {
        super(sessionService);
        this.projectEndpoint = projectEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list";
    }

    @Override
    public void execute() {
        System.out.println("[LIST PROJECTS]");
        @Nullable final SessionDTO sessionDTO = sessionService.getCurrentSession();
        @NotNull final List<ProjectDTO> projectsDTO = projectEndpoint.findAllProjects(sessionDTO);
        int index = 1;
        for (ProjectDTO projectDTO: projectsDTO) {
            System.out.println(index + ". " + projectDTO.getName());
            index++;
        }
        System.out.println("[OK]");
    }
}
