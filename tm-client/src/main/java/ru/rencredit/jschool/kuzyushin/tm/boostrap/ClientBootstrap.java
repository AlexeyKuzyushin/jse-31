package ru.rencredit.jschool.kuzyushin.tm.boostrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.exception.system.IncorrectArgException;
import ru.rencredit.jschool.kuzyushin.tm.exception.system.IncorrectCmdException;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

import java.lang.Exception;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Component
public class ClientBootstrap {

    @Getter
    @NotNull
    @Autowired
    private List<AbstractCommand> commandList;

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    private void initCommands(@NotNull final List<AbstractCommand> commandList) {
        for (@NotNull AbstractCommand command: commandList) init(command);
    }

    private void init(@NotNull final AbstractCommand command) {
        commands.put(command.name(), command);
        arguments.put(command.arg(), command);
    }

    public void run(@Nullable final String[] args){
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        initCommands(commandList);
        if (parseArgs(args)) System.exit(0);
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                logError(e);
            }
        }
    }

    private static void logError(Exception e) {
        System.err.println(e.getMessage());
        System.err.println("[FAILED]");
    }

    private boolean parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        try {
            parseArg(arg);
        } catch (Exception e) {
            logError(e);
        }
        return true;
    }

    @SneakyThrows
    private void parseArg(@NotNull final String arg) {
        if (arg.isEmpty()) return;
        @Nullable final AbstractCommand argument = arguments.get(arg);
        if (argument == null) throw new IncorrectArgException(arg);
        argument.execute();
    }

    @SneakyThrows
    private void parseCommand(@Nullable final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        @Nullable final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new IncorrectCmdException(cmd);
        command.execute();
    }
}

