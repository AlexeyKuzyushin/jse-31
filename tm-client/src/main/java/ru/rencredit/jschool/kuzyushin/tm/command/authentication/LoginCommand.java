package ru.rencredit.jschool.kuzyushin.tm.command.authentication;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

@Component
public final class LoginCommand extends AbstractCommand {

    @NotNull
    private final SessionEndpoint sessionEndpoint;

    @Autowired
    public LoginCommand(
            final @NotNull SessionEndpoint sessionEndpoint,
            final @NotNull ISessionService sessionService
    ) {
        super(sessionService);
        this.sessionEndpoint = sessionEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "login";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Login";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        @Nullable final SessionDTO sessionDTO = sessionEndpoint.openSession(login, password);
        sessionService.setCurrentSession(sessionDTO);
        System.out.println("[OK]");
    }
}
