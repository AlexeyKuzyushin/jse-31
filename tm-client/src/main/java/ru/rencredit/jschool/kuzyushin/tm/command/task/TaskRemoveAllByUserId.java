package ru.rencredit.jschool.kuzyushin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.TaskEndpoint;

@Component
public final class TaskRemoveAllByUserId extends AbstractCommand {

    @NotNull
    private final TaskEndpoint taskEndpoint;

    @Autowired
    public TaskRemoveAllByUserId(
            final @NotNull TaskEndpoint taskEndpoint,
            final @NotNull ISessionService sessionService
    ) {
        super(sessionService);
        this.taskEndpoint = taskEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "task-remove-all-by-userId";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all tasks of user";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        @Nullable final SessionDTO sessionDTO = sessionService.getCurrentSession();
        taskEndpoint.removeAllTasksByUserId(sessionDTO);
        System.out.println("[OK]");
    }
}
