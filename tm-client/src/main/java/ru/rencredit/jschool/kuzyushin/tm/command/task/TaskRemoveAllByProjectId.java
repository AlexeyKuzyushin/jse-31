package ru.rencredit.jschool.kuzyushin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.TaskEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

@Component
public final class TaskRemoveAllByProjectId extends AbstractCommand {

    @NotNull
    private final TaskEndpoint taskEndpoint;

    @Autowired
    public TaskRemoveAllByProjectId(
            final @NotNull TaskEndpoint taskEndpoint,
            final @NotNull ISessionService sessionService
    ) {
        super(sessionService);
        this.taskEndpoint = taskEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "task-remove-all-by-projectId";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all tasks of project";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER PROJECTID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @Nullable final SessionDTO sessionDTO = sessionService.getCurrentSession();
        taskEndpoint.removeAllTasksByProjectId(sessionDTO, projectId);
        System.out.println("[OK]");
    }
}
