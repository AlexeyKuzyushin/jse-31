package ru.rencredit.jschool.kuzyushin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.UserEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

@Component
public final class UserRemoveByIdCommand extends AbstractCommand {

    @NotNull
    private final UserEndpoint userEndpoint;

    @Autowired
    public UserRemoveByIdCommand(
            final @NotNull UserEndpoint userEndpoint,
            final @NotNull ISessionService sessionService
    ) {
        super(sessionService);
        this.userEndpoint = userEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "user-remove-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove user by id";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER]");
        @Nullable final SessionDTO sessionDTO = sessionService.getCurrentSession();
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        userEndpoint.removeUserById(sessionDTO, id);
        System.out.println("[OK]");
    }
}
