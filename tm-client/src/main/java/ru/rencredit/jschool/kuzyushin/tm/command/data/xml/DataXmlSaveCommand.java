package ru.rencredit.jschool.kuzyushin.tm.command.data.xml;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.DataEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;

@Component
public class DataXmlSaveCommand extends AbstractCommand {

    @NotNull
    private final DataEndpoint dataEndpoint;

    @Autowired
    public DataXmlSaveCommand(
            final @NotNull DataEndpoint dataEndpoint,
            final @NotNull ISessionService sessionService
    ) {
        super(sessionService);
        this.dataEndpoint = dataEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "data-xml-save";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to xml file";
    }


    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA XML SAVE]");
        @Nullable final SessionDTO sessionDTO = sessionService.getCurrentSession();
        dataEndpoint.saveDataXml(sessionDTO);
        System.out.println("[OK]");
    }
}
