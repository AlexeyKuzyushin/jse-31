package ru.rencredit.jschool.kuzyushin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;

@Component
public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String description() {
        return "Show developer info";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Alexey Kuzyushin");
        System.out.println("E-MAIL: alexeykuzyushin@yandex.ru");
    }
}
