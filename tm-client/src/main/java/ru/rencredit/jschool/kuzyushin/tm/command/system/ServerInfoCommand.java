package ru.rencredit.jschool.kuzyushin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.ProjectEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionEndpoint;

@Component
public final class ServerInfoCommand extends AbstractCommand {

    @NotNull
    private final SessionEndpoint sessionEndpoint;

    @Autowired
    public ServerInfoCommand(
            final @NotNull SessionEndpoint sessionEndpoint,
            final @NotNull ISessionService sessionService
    ) {
        super(sessionService);
        this.sessionEndpoint = sessionEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "server-info";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show information about connection port and host";
    }

    @Override
    public void execute() {
        System.out.println("[SERVER INFO]");
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        @NotNull final String host = sessionEndpoint.getServerHost(session);
        @NotNull final Integer port = sessionEndpoint.getServerPort(session);
        System.out.println("HOST: " + host);
        System.out.println("PORT: " + port);
        System.out.println("[OK]");
    }
}
