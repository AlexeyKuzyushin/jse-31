package ru.rencredit.jschool.kuzyushin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.ProjectEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;

@Component
public final class ProjectCountCommand extends AbstractCommand {

    @NotNull
    private final ProjectEndpoint projectEndpoint;

    @Autowired
    public ProjectCountCommand(
            final @NotNull ProjectEndpoint projectEndpoint,
            final @NotNull ISessionService sessionService
    ) {
        super(sessionService);
        this.projectEndpoint = projectEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "project-count";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Count all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[COUNT PROJECTS]");
        @Nullable final SessionDTO sessionDTO = sessionService.getCurrentSession();
        System.out.println("COUNT: " + projectEndpoint.countAllProjects(sessionDTO));
        System.out.println("[OK]");
    }
}
