package ru.rencredit.jschool.kuzyushin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.ProjectEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.sql.Date;
import java.text.ParseException;
import java.util.GregorianCalendar;

@Component
public final class ProjectUpdateFinishDate extends AbstractCommand {

    @NotNull
    private final ProjectEndpoint projectEndpoint;

    @Autowired
    public ProjectUpdateFinishDate(
            final @NotNull ProjectEndpoint projectEndpoint,
            final @NotNull ISessionService sessionService
    ) {
        super(sessionService);
        this.projectEndpoint = projectEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "project-update-finish-date";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update finish date of project";
    }

    @Override
    public void execute() throws ParseException, DatatypeConfigurationException {
        System.out.println("[UPDATE FINISH DATE]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("ENTER DATE (dd-MMM-yyyy):");
        final Date newDate = Date.valueOf(TerminalUtil.nextLine());
        final GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(newDate);
        final XMLGregorianCalendar xmlGregDate =  DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
        @Nullable final SessionDTO sessionDTO = sessionService.getCurrentSession();
        projectEndpoint.updateProjectFinishDate(sessionDTO, id, xmlGregDate);
        System.out.println("[OK]");
    }
}
