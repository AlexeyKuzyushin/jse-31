package ru.rencredit.jschool.kuzyushin.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.service.SessionService;

public abstract class AbstractCommand {

    @Nullable
    protected ISessionService sessionService;

    @Autowired
    public AbstractCommand(final @NotNull ISessionService sessionService) {
        this.sessionService = sessionService;
    }

    public AbstractCommand() {

    }

    @NotNull
    public abstract String name();

    @Nullable
    public abstract String arg();

    @NotNull
    public abstract String description();

    public abstract void execute() throws Exception;
}
