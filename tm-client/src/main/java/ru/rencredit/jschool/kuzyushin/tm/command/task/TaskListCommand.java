package ru.rencredit.jschool.kuzyushin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.TaskDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.TaskEndpoint;

import java.util.List;

@Component
public final class TaskListCommand extends AbstractCommand {

    @NotNull
    private final TaskEndpoint taskEndpoint;

    @Autowired
    public TaskListCommand(
            final @NotNull TaskEndpoint taskEndpoint,
            final @NotNull ISessionService sessionService
    ) {
        super(sessionService);
        this.taskEndpoint = taskEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list";
    }

    @Override
    public void execute() {
        System.out.println("[LIST TASKS]");
        @Nullable final SessionDTO sessionDTO = sessionService.getCurrentSession();
        @Nullable final List<TaskDTO> tasksDTO = taskEndpoint.findAllTasks(sessionDTO);
        int index = 1;
        for (TaskDTO taskDTO: tasksDTO) {
            System.out.println(index + ". " + taskDTO.getName());
            index++;
        }
        System.out.println("[OK]");
    }
}
