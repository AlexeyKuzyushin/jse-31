package ru.rencredit.jschool.kuzyushin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;

@Component
public final class ExitCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name()  {
        return "exit";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description()   {
        return "Close application";
    }

    @Override
    public void execute() {
        System.exit(0);
    }
}
