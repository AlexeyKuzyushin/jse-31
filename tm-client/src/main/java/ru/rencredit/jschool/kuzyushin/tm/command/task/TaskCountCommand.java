package ru.rencredit.jschool.kuzyushin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.TaskEndpoint;

@Component
public final class TaskCountCommand extends AbstractCommand {

    @NotNull
    private final TaskEndpoint taskEndpoint;

    @Autowired
    public TaskCountCommand(
            final @NotNull TaskEndpoint taskEndpoint,
            final @NotNull ISessionService sessionService
    ) {
        super(sessionService);
        this.taskEndpoint = taskEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "task-count";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Count all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[COUNT TASKS]");
        @Nullable final SessionDTO sessionDTO = sessionService.getCurrentSession();
        System.out.println("COUNT: " + taskEndpoint.countAllTasks(sessionDTO));
        System.out.println("[OK]");
    }
}
