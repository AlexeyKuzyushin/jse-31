package ru.rencredit.jschool.kuzyushin.tm.command.authentication;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionEndpoint;

@Component
public final class LogoutCommand extends AbstractCommand {

    @NotNull
    private final SessionEndpoint sessionEndpoint;

    @Autowired
    public LogoutCommand(
            final @NotNull SessionEndpoint sessionEndpoint,
            final @NotNull ISessionService sessionService
    ) {
        super(sessionService);
        this.sessionEndpoint = sessionEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Logout";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        @Nullable final SessionDTO sessionDTO = sessionService.getCurrentSession();
        sessionEndpoint.closeSession(sessionDTO);
        sessionService.clearCurrentSession();
        System.out.println("[OK]");
    }
}
