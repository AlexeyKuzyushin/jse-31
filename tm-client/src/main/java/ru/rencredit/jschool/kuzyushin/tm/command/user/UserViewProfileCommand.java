package ru.rencredit.jschool.kuzyushin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.UserDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.UserEndpoint;

@Component
public final class UserViewProfileCommand extends AbstractCommand {

    @NotNull
    private final UserEndpoint userEndpoint;

    @Autowired
    public UserViewProfileCommand(
            final @NotNull UserEndpoint userEndpoint,
            final @NotNull ISessionService sessionService
    ) {
        super(sessionService);
        this.userEndpoint = userEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "user-view-profile";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show user profile";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW USER PROFILE]");
        @Nullable final SessionDTO sessionDTO = sessionService.getCurrentSession();
        @Nullable final UserDTO userDTO = userEndpoint.viewUserProfile(sessionDTO);
        if (userDTO == null) return;
        System.out.println("ID: " + userDTO.getId());
        System.out.println("LOGIN: " + userDTO.getLogin());
        System.out.println("E-mail: " + userDTO.getEmail());
        System.out.println("FIRST NAME: " + userDTO.getFirstName());
        System.out.println("LAST NAME: " + userDTO.getLastName());
        System.out.println("MIDDLE NAME: " + userDTO.getMiddleName());
        System.out.println("[OK]");
    }
}
