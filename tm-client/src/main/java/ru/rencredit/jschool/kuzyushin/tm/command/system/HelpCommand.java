package ru.rencredit.jschool.kuzyushin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.boostrap.ClientBootstrap;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;

import java.util.List;

@Component
public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private ClientBootstrap clientBootstrap;

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @NotNull
    @Override
    public String description() {
        return "Display terminal commands";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final List<AbstractCommand> commands = clientBootstrap.getCommandList();
        for (@NotNull final AbstractCommand command: commands) {
            StringBuilder result = new StringBuilder();
            command.name();
            if (!command.name().isEmpty()) result.append(command.name());
            if (command.arg() != null && !command.arg().isEmpty()) result.append(", ").append(command.arg());
            command.description();
            if (!command.description().isEmpty())
                result.append(": ").append(command.description());
            System.out.println(result.toString());
        }
        System.out.println("[OK]");
    }
}
